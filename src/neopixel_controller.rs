use paho_mqtt as mqtt;
use serde_json::json;
use std::process;
const QOS: i32 = 1;

pub fn set_color(red: &str, green: &str, blue: &str) {
    let host = "ws://192.168.1.204:8001".to_string();

    let cli = mqtt::AsyncClient::new(host).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    let conn_opts = mqtt::ConnectOptions::new();

    if let Err(e) = cli.connect(conn_opts).wait() {
        println!("Unable to connect: {:?}", e);
        process::exit(1);
    }

    let topic = mqtt::Topic::new(&cli, "led/SetColor", QOS);

    let command = json!({
       "red": red,
       "green": green,
       "blue": blue
    });

    let tok = topic.publish(command.to_string());

    if let Err(e) = tok.wait() {
        println!("Error sending message: {:?}", e);
    }

    // Disconnect from the broker
    let tok = cli.disconnect(None);
    tok.wait().unwrap();
}

pub fn set_brightness(brightness: &str) {
    let host = "ws://192.168.1.204:8001".to_string();

    let cli = mqtt::AsyncClient::new(host).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    let conn_opts = mqtt::ConnectOptions::new();

    if let Err(e) = cli.connect(conn_opts).wait() {
        println!("Unable to connect: {:?}", e);
        process::exit(1);
    }

    let topic = mqtt::Topic::new(&cli, "led/SetBrightness", QOS);

    let tok = topic.publish(brightness);

    if let Err(e) = tok.wait() {
        println!("Error sending message: {:?}", e);
    }

    // Disconnect from the broker
    let tok = cli.disconnect(None);
    tok.wait().unwrap();
}
