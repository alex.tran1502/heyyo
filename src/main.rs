mod crypto;
mod neopixel_controller;

use clap::{App, AppSettings, Arg};
use neopixel_controller::{set_brightness, set_color};
use reqwest;
use serde_json::Value;
use std::collections::HashMap;
use termion::color::{AnsiValue, Color, Fg};
use termion::{color, style};

fn is_u8(v: &str) -> Result<(), String> {
    let value: u32 = v.parse::<u32>().unwrap();

    if value <= 256 {
        return Ok(());
    }

    Err(String::from("Value must be between 0 and 256"))
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("HeyYo")
        .setting(AppSettings::ArgRequiredElseHelp)
        /* Neo Pixel Controller */
        .subcommand(
            App::new("neopixel")
                .setting(AppSettings::ArgRequiredElseHelp)
                .about("NeoPixel Controller")
                .subcommand(
                    App::new("color")
                        .about("Change Color")
                        .arg(
                            Arg::new("red")
                                .about("Red Color")
                                .index(1)
                                .required(true)
                                .validator(is_u8),
                        )
                        .arg(
                            Arg::new("green")
                                .about("Red Color")
                                .index(2)
                                .required(true)
                                .validator(is_u8),
                        )
                        .arg(
                            Arg::new("blue")
                                .about("Red Color")
                                .index(3)
                                .required(true)
                                .validator(is_u8),
                        ),
                )
                .subcommand(
                    App::new("brightness").about("Change Brightness").arg(
                        Arg::new("brightness")
                            .about("Red Color")
                            .index(1)
                            .required(true),
                    ),
                ),
        )
        .subcommand(
            App::new("coin").about("Show crypto price").arg(
                Arg::new("coin_name")
                    .about("Coin Name or ID (bitcoin, cardano...)")
                    .index(1)
                    .required(true),
            ),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("neopixel", neopixel_matches)) => match neopixel_matches.subcommand() {
            Some(("color", color_matches)) => {
                set_color(
                    color_matches.value_of("red").unwrap(),
                    color_matches.value_of("green").unwrap(),
                    color_matches.value_of("blue").unwrap(),
                );
                Ok(())
            }
            Some(("brightness", brightness_matches)) => {
                set_brightness(brightness_matches.value_of("brightness").unwrap());
                Ok(())
            }
            _ => unreachable!(),
        },
        Some(("coin", coin_matches)) => {
            let mut coin_name = coin_matches.value_of("coin_name").unwrap();

            if coin_name == "ada" {
                coin_name = "cardano"
            }
            let url = format!("https://api.coingecko.com/api/v3/simple/price?ids={}&vs_currencies=usd&include_24hr_change=true", coin_name);

            let resp = reqwest::blocking::get(url)?.json::<HashMap<String, Value>>()?;

            let price = resp[coin_name]["usd"].as_f64().unwrap();
            let changes = resp[coin_name]["usd_24h_change"].as_f64().unwrap();

            println!(
                "Current Price: {}{}${}{}",
                style::Bold,
                color::Fg(color::Green),
                price,
                style::Reset
            );
            println!("24-hour Change: {}{:.2?}%", color::Fg(color::Blue), changes);

            Ok(())
        }
        _ => unreachable!(),
    }
}
